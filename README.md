# Radiation Damage Calculation



## Getting started

To run the code one needs to dowload the repository and on an active root pront just type:
.x Run.C

No other prerequisits are necessery apart from root and an active c++ compiler. The code runs in all systems (Windows/Linux) following the POSIX standard and is tested with root 6. 

All available options can be set through the steering macro (Run.C) where inlined comments detail all implemented switches.

Please note that the scenario files within the "scenarios" folder only correspond to LHCb VELO and one might need to adjust the fluence estimation function to the relevent experiment. If you would like a specific fluence estimation fucntion implemented, please contact me and it can be included.

Radiation/annealing scenarios are given in the format of a four column tabulated text with the first column corresponding to the duration of each sstep, the second to the temperature, the third to the distance from the closed position (only for LHCb VELO - to be adjusted for other experiments) and the forth to the collected luminocity.The code will go through all available .txt files in the specified input folder and estimate the corresponding quantities using the name of each txt file as the scenario name for the legends on the output plots.

The shared folder contains data from bibliography (see presentation on pdf file at main folder) for acceptor removal coefficient fits. A coresponding donor removal reference dataset is under implementation. Within the shared folder, data from a reference detector (depletion voltage - fluence) can also be found and are used for the stimation of the re-introduction coeficients and the removal fraction.

The code supports both dynamic logarithmic annealing for the N-effective as well as a static model, which can be selsected by a flag within Run.C.

In case of requests, bugs or additional features, contact Vagelis Gkougkousis (egkougko@cern.ch)

Based on an original version from Julien Beyer and implemented on the CERN Yellow Report.